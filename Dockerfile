FROM python:3.8-alpine

WORKDIR /errors

COPY requirements /tmp/requirements
RUN pip install -r /tmp/requirements \
    && mkdir -p responses/

COPY errors_handler/ errors_handler/
COPY templates/ templates/
COPY static/ static/

EXPOSE 80
CMD [ "uvicorn", "errors_handler.api:api", "--host", "0.0.0.0", "--port", "80"]
