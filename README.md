## HTTP errors handler

*This service provides an API to display friendly error responses based on given status codes*

## Usage

Example with docker-compose

```yaml
services:
  errors_handler:
    image: sylvanld/http-errors-handler:0.0.1
    environment:
      DEFAULT_RESPONSE_FORMAT: json
      SUGGESTED_URL: https://sylvan.ovh
    ports:
      - 8080:80
```

## Configuration

You can set the following environment variables to configure this service.

|Variable|Description|Default|
|-|-|-|
|DEFAULT_RESPONSE_FORMAT|Format of the default response (one of 'json', 'html').|html|
|SUGGESTED_URL|URL user will be suggested on the error page.|-|
