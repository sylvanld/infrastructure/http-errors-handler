STATUS_INFO = {
    400: {
        "status": 400,
        "short_description": "Bad Request",
        "description": "The request cannot be fulfilled due to bad syntax",
    },
    401: {
        "status": 401,
        "short_description": "Unauthorized",
        "description": "The request was a legal request, but the server is refusing to respond to it. For use when authentication is possible but has failed or not yet been provided",
    },
    402: {
        "status": 402,
        "short_description": "Payment Required",
        "description": "Reserved for future use",
    },
    403: {
        "status": 403,
        "short_description": "Forbidden",
        "description": "The request was a legal request, but the server is refusing to respond to it",
    },
    404: {
        "status": 404,
        "short_description": "Not Found",
        "description": "The requested page could not be found but may be available again in the future",
    },
    405: {
        "status": 405,
        "short_description": "Method Not Allowed",
        "description": "A request was made of a page using a request method not supported by that page",
    },
    406: {
        "status": 406,
        "short_description": "Not Acceptable",
        "description": "The server can only generate a response that is not accepted by the client",
    },
    407: {
        "status": 407,
        "short_description": "Proxy Authentication Required",
        "description": "The client must first authenticate itself with the proxy",
    },
    408: {
        "status": 408,
        "short_description": "Request Timeout",
        "description": "The server timed out waiting for the request",
    },
    409: {
        "status": 409,
        "short_description": "Conflict",
        "description": "The request could not be completed because of a conflict in the request",
    },
    410: {
        "status": 410,
        "short_description": "Gone",
        "description": "The requested page is no longer available",
    },
    411: {
        "status": 411,
        "short_description": "Length Required",
        "description": 'The "Content-Length" is not defined. The server will not accept the request without it ',
    },
    412: {
        "status": 412,
        "short_description": "Precondition Failed",
        "description": "The precondition given in the request evaluated to false by the server",
    },
    413: {
        "status": 413,
        "short_description": "Request Too Large",
        "description": "The server will not accept the request, because the request entity is too large",
    },
    414: {
        "status": 414,
        "short_description": "Request-URI Too Long",
        "description": "The server will not accept the request, because the URI is too long. Occurs when you convert a POST request to a GET request with a long query information ",
    },
    415: {
        "status": 415,
        "short_description": "Unsupported Media Type",
        "description": "The server will not accept the request, because the media type is not supported ",
    },
    416: {
        "status": 416,
        "short_description": "Range Not Satisfiable",
        "description": "The client has asked for a portion of the file, but the server cannot supply that portion",
    },
    417: {
        "status": 417,
        "short_description": "Expectation Failed",
        "description": "The server cannot meet the requirements of the Expect request-header field",
    },
    500: {
        "status": 500,
        "short_description": "Internal Server Error",
        "description": "A generic error message, given when no more specific message is suitable",
    },
    501: {
        "status": 501,
        "short_description": "Not Implemented",
        "description": "The server either does not recognize the request method, or it lacks the ability to fulfill the request",
    },
    502: {
        "status": 502,
        "short_description": "Bad Gateway",
        "description": "The server was acting as a gateway or proxy and received an invalid response from the upstream server",
    },
    503: {
        "status": 503,
        "short_description": "Service Unavailable",
        "description": "The server is currently unavailable (overloaded or down)",
    },
    504: {
        "status": 504,
        "short_description": "Gateway Timeout",
        "description": "The server was acting as a gateway or proxy and did not receive a timely response from the upstream server",
    },
    505: {
        "status": 505,
        "short_description": "HTTP Version Not Supported",
        "description": "The server does not support the HTTP protocol version used in the request",
    },
    511: {
        "status": 511,
        "short_description": "Network Authentication Required",
        "description": "The client needs to authenticate to gain network access",
    },
    526: {
        "status": 526,
        "short_description": "Service does not exists",
        "description": "You are trying to access a service that does not exists",
    },
}
