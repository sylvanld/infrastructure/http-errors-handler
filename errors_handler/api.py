from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles

from errors_handler.response import make_response

api = FastAPI()
api.mount("/static", StaticFiles(directory="static"), name="static")


@api.get("/error/{status}")
async def make_error_response(request: Request, status: int, format: str = None):
    return make_response(request, status, format=format)
