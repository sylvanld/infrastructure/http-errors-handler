import base64
import json
import logging
import os

import jinja2
from fastapi import HTTPException, Request
from fastapi.responses import HTMLResponse, Response
from fastapi.templating import Jinja2Templates
from jinja2 import Template

from errors_handler.settings import DEFAULT_RESPONSE_FORMAT, SUGGESTED_URL
from errors_handler.status import STATUS_INFO

LOGGER = logging.getLogger(__name__)

HTML_DEFAULT_ERROR_TEMPLATE = Template(
    """
<main style="width: 50em">
<h2>ERROR - {{ error.short_description }}</h2>

<p>{{ error.description }}</p>

{% if suggested_url %}
    Continue to <a href="{{suggested_url}}">{{suggested_url}}</a>
{% endif %}
</main>
"""
)


def embed_image(image_relative_path: str):
    """Allow embeding an image in base64 in jinja templates."""
    with open(os.path.join("static", image_relative_path), "rb") as image_file:
        return "data:image/png;base64, " + base64.b64encode(image_file.read()).decode(
            "utf-8"
        )


TEMPLATES_CONTEXT = Jinja2Templates("templates/")
TEMPLATES_CONTEXT.env.globals.update(embed_image=embed_image)


def make_json_response(error: dict, **context):
    """Return a JSON error response that can easily be handled by scripts."""
    if context:
        error.update(context=context)
    return Response(
        json.dumps(error, indent=4),
        status_code=error["status"],
        headers={"Content-Type": "application/json"},
    )


def make_html_response(error: dict, **context):
    """Return a human readable error message."""
    return HTMLResponse(
        HTML_DEFAULT_ERROR_TEMPLATE.render(error=error, **context),
        status_code=error["status"],
    )


def make_template_response(request: Request, error: dict, **context):
    """Return a custom templated html response used to display user friendly messages."""
    return TEMPLATES_CONTEXT.TemplateResponse(
        f"{error['status']}.html",
        {"request": request, "error": error, **context},
        status_code=error["status"],
    )


def make_response(request: Request, status: int, format: str = None):
    """Return appropriate response given error and specified format."""
    if format is None:
        format = DEFAULT_RESPONSE_FORMAT

    context = {}
    if SUGGESTED_URL is not None:
        context["suggested_url"] = SUGGESTED_URL

    try:
        error = STATUS_INFO[status]
    except KeyError:
        error = STATUS_INFO[500]

    if format == "html":
        return make_html_response(error, **context)
    elif format == "json":
        return make_json_response(error, **context)
    elif format == "template":
        try:
            return make_template_response(request, error, **context)
        except jinja2.exceptions.TemplateNotFound as error:
            fallback_format = DEFAULT_RESPONSE_FORMAT
            if fallback_format == "template":
                fallback_format = "html"

            LOGGER.warning(
                "Template not found, falling back to format %s",
                fallback_format,
                exc_info=error,
            )

            return make_response(request, status, fallback_format)
    else:
        raise HTTPException(400, detail=f"Unsupported response format {format}")
